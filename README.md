**NPM-VAGRANT SETUP**
=====================

### Problem: ###
NPM throws error code -5, which is an ambigious error but in our case 
all it generally means is that npm is trying to create / update / delete
files way faster than vagrant box can handle (because of it async nature).
So technically, it becomes impossible to use vagrant with windows for
projects to do with NODE / NPM.

### Solution: ###
Issue can be resolved if we slow down the NPM I/O operation. But that is
not possible but a work around this is to break the I/O operation into
multiple chunks, to make each operation smaller than buffer size. I have
tested this approach and it works. There is a rare chance of you getting
warning or error about name update failure but you can safely ignore it 
as it should not affect working of NPM package.

### Pre Requisites ###
**Please install the following on your pc first**

1. Virtualbox with extensions
2. Vagrant
3. Vagrant Plugins
    * rsync@1.0.9
    * vagrant-winnfsd@1.2.1

### Steps ###
Please follow the instructions below to have a proper working vagrant 
environment, feel free to use this vagrant script to power your devbox.
This repository has a test project in dev folder. This project has a 
file called cmd in bin folder. 

1. Provision the vagrant box
2. Once provision is complete, its a good idea to reload the box
3. Update bin/cmd file to add your files and its versions
4. run `bin/cmd ni`

This should remove all existing dependencies and add it a fresh. If you 
are looking for adding one or more additional package after having your 
setup completed then it makes sense to run that command individually and 
add the dependency into the file "bin/cmd"

`npm install package-name@version --no-bin-links --save-dev`