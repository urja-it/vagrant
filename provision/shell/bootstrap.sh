#!/usr/bin/env bash

#####NOTE######
# On windows never use symlinks
# eg sudo npm install --no-bin-links for npm packages
# or don't use --symlink on composer or symfony packages
###############

# recording original directory
ORIG_DIR="$PWD"

#PREVENTING IDLE DISCONNECTS
net config server /autodisconnect:-1

# Setting up the system
sudo apt-get update 2> /dev/null

#Remove unwanted softwares
sudo apt-get purge -y ubuntu-desktop 2> /dev/null
sudo apt-get purge -y thunderbird pidgin gmusicbrowser 2> /dev/null
sudo apt-get autoremove -y 2> /dev/null

#Setup dev environment
sudo apt-get install -y curl 2> /dev/null
sudo apt-get install -y make 2> /dev/null
sudo apt-get install -y vim 2> /dev/null
sudo apt-get install -y php php-pear php-dev php-gd php-curl php-mcrypt php-memcached 2> /dev/null
sudo apt-get install -y php-xdebug 2> /dev/null
sudo apt-get install -y apache2 libapache2-mod-php 2> /dev/null
sudo apt-get install -y openssl 2> /dev/null
sudo apt-get install -y git 2> /dev/null
sudo apt-get install -y subversion 2> /dev/null
sudo apt-get install -y php-mbstring 2> /dev/null
sudo apt-get install -y libnotify-bin 2> /dev/null

# Installing nodejs, npm and bower
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash - 2> /dev/null
sudo apt-get install nodejs -y
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo "export PATH=~/.npm-global/bin:$PATH" >> ~/.profile
source ~/.profile
npm install -g npm 2> /dev/null
npm install -g bower 2> /dev/null
npm install -g gulp-cli 2> /dev/null
sudo ln -sT ~/.npm-global/bin/bower /usr/bin/bower 2> /dev/null
sudo ln -sT ~/.npm-global/bin/npm /usr/bin/npm 2> /dev/null
sudo ln -sT ~/.npm-global/bin/gulp /usr/bin/gulp 2> /dev/null

# Codeception
sudo curl -LsS http://codeception.com/codecept.phar -o /usr/local/bin/codecept
sudo chmod a+x /usr/local/bin/codecept

# required dependency for modifying files using sed
sudo apt install iprint -y

# not strictly required but good for stopping npm permission issues
sudo chown vagrant:vagrant -R /usr/local/

# Moving back to the original directory
cd "$ORIG_DIR"

sudo a2enmod rewrite 2> /dev/null

APACHEUSR=`grep -c 'APACHE_RUN_USER=www-data' /etc/apache2/envvars`
APACHEGRP=`grep -c 'APACHE_RUN_GROUP=www-data' /etc/apache2/envvars`
if [ APACHEUSR ]; then
    sudo sed -i 's/APACHE_RUN_USER=www-data/APACHE_RUN_USER=vagrant/' /etc/apache2/envvars
fi
if [ APACHEGRP ]; then
    sudo sed -i 's/APACHE_RUN_GROUP=www-data/APACHE_RUN_GROUP=vagrant/' /etc/apache2/envvars
fi
sudo chown -R vagrant:www-data /var/lock/apache2

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password ROOTPASSWORD'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password ROOTPASSWORD'
sudo apt-get install -y mysql-server php-mysql 2> /dev/null
sudo apt-get install -y mysql-client 2> /dev/null

# Making sure the db is accessible from any network
sudo sed -i 's/^bind-address.*/# bind-address = 127.0.0.1/' /etc/mysql/mysql.conf.d/mysqld.cnf
sudo sed -i 's/^skip-external-locking/# skip-external-locking/' /etc/mysql/mysql.conf.d/mysqld.cnf

sudo sed -i 's/^\[mysqld\]/[mysqld]\n#\n# Making sure UTF8MB4 is default\n#\ncharacter-set-server=utf8mb4\ncollation-server=utf8mb4_general_ci\nskip-character-set-client-handshake\n/' /etc/mysql/my.cnf


if [ ! -f /var/log/dbinstalled ];
then
    echo "CREATE USER 'vagrant'@'localhost' IDENTIFIED BY 'VAGRANTPASSWORD'" | mysql -uroot -pROOTPASSWORD
	echo "GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'localhost' WITH GRANT OPTION" | mysql -uroot -pROOTPASSWORD
	echo "CREATE USER 'vagrant'@'192.168.56.1' IDENTIFIED BY 'VAGRANTPASSWORD'" | mysql -uroot -pROOTPASSWORD
	echo "GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'192.168.56.1' WITH GRANT OPTION" | mysql -uroot -pROOTPASSWORD
    echo "CREATE USER 'vagrant'@'%' IDENTIFIED BY 'VAGRANTPASSWORD'" | mysql -uroot -pROOTPASSWORD
    echo "GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'%' WITH GRANT OPTION" | mysql -uroot -pROOTPASSWORD
    echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'" | mysql -uroot -pROOTPASSWORD
    echo "flush privileges" | mysql -uroot -pROOTPASSWORD
    sudo touch /var/log/dbinstalled

	# implement logic for selecting database in the sql or specifying here correctly
	#if [ -f /vagrant/data/initial.sql ];
    #then
    #    mysql -uroot -pROOTPASSWORD internal < /vagrant/data/initial.sql
    #fi
fi

# chdir to tmp folder
cd /tmp

# Installing PHP Unit
wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit

# Installing composer
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
sudo mv composer.phar /usr/local/bin/composer

# Installing symfony
php -r "file_put_contents('symfony', file_get_contents('https://symfony.com/installer'));"
chmod +x symfony
sudo mv symfony /opt/dev/www/symfony

# Moving back to the original directory
cd "$ORIG_DIR"

# if /var/www is not a symlink then create the symlink and set up apache
if [ ! -h /var/www ];
then
    rm -rf /var/www
    ln -fs /opt/dev/www /var/www
    sudo a2enmod rewrite 2> /dev/null
    sudo sed -i '/AllowOverride None/c AllowOverride All' /etc/apache2/sites-available/000-default.conf
    sudo service apache2 restart 2> /dev/null
fi

#Adding the server name attribute to apache2.conf
sudo sed -i '$ a\ServerName vagrant' /etc/apache2/apache2.conf

# Updating php.ini for apache and commandline
# creating an array with config file names for php
PHPINI=(/etc/php/7.0/cli/php.ini /etc/php/7.0/apache2/php.ini)

# Creating directories for uploads and sessions for php
mkdir /var/phpuploads /var/phpsession
sudo chown vagrant:vagrant /var/phpuploads /var/phpsession

for config in ${PHPINI[@]}; do
    sudo sed -i 's/^short_open_tag.*/short_open_tag=Off/' $config
    sudo sed -i 's/^zend.enable_gc.*/zend.enable_gc=On/' $config
    sudo sed -i 's/^error_reporting.*/error_reporting=E_ALL/' $config
    sudo sed -i 's/^display_errors.*/display_errors=On/' $config
    sudo sed -i 's/^log_errors.*/log_errors=On/' $config
    sudo sed -i 's/^post_max_size.*/post_max_size=128M/' $config
    sudo sed -i 's/^;upload_tmp_dir.*/upload_tmp_dir="\/tmp\/phpuploads"/' $config
    sudo sed -i 's/^upload_max_filesize.*/upload_max_filesize=128M/' $config
    sudo sed -i 's/^;date.timezone.*/date.timezone=Europe\/London/' $config
    sudo sed -i 's/^;session.save_path.*/session.save_path="\/var\/phpsession"/' $config
done


# Adding xdebug information for commandline and apache
# creating an array with config file names for php
XDEBUGINI=(/etc/php/7.0/cli/conf.d/20-xdebug.ini /etc/php/7.0/apache2/conf.d/20-xdebug.ini)

for config in ${XDEBUGINI[@]}; do
    sudo sed -i '$ a\xdebug.remote_enable=on' $config
    sudo sed -i '$ a\xdebug.remote_connect_back=on' $config
    sudo sed -i '$ a\xdebug.remote_handler=dbgp' $config
    sudo sed -i '$ a\xdebug.remote_mode=req' $config
    sudo sed -i '$ a\xdebug.remote_port=9000' $config
    sudo sed -i '$ a\xdebug.profiler_enable_trigger=on' $config
    sudo sed -i '$ a\xdebug.profiler_output_dir="./profiler"' $config
    sudo sed -i '$ a\xdebug.profiler_output_name="cachegrind.out.%t"' $config
    sudo sed -i '$ a\xdebug.file_link_format = "file:///openLocalFile/%f?lineNumber=%l"' $config
    sudo sed -i '$ a\xdebug.idekey=superdebug' $config
    sudo sed -i '$ a\xdebug.remote_autostart=off' $config
    sudo sed -i '$ a\xdebug.var_display_max_depth=5' $config
    sudo sed -i '$ a\xdebug.max_nesting_level=250' $config
done

# Removing the default server
sudo rm -rf /etc/apache2/sites-enabled/000-default.conf

# restart apache
sudo service apache2 reload 2> /dev/null

# Important commands
sudo ln -sT /usr/bin/nodejs /usr/bin/node
sudo mount -a